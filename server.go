package main

import (
	"fmt"
	"encoding/json"
	"net/http"
	"path"
)

func main(){
	http.HandleFunc("/", HelloServer)
	http.HandleFunc("/json", JsonServer)
	http.HandleFunc("/file", FileServer)
	http.ListenAndServe(":3008", nil)
}

func HelloServer(w http.ResponseWriter, r *http.Request){
	fmt.Fprintf(w, "%s", "Something")
}

func JsonServer(w http.ResponseWriter, r *http.Request){
	name := r.URL.Query()["name"]
	if name == nil {
		name = []string{"Default Name"}
	}
	js := map[string]string{"Name": name[0]}
	
	w.Header().Set("Content-Type", "application/json")
	json, err := json.Marshal(js)

	if err!= nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Write(json)
}

func FileServer(w http.ResponseWriter, r *http.Request){
	fp := path.Join("", "text.txt")
	http.ServeFile(w, r, fp)
}
